# ¿Qué es Swarm?

# El problema de la escala: qué pasa cuando una computadora sóla no alcanza

La escalabilidad es el poder aumentar la capacidad de potencia de computo para poder servir a más usuarios o a procesos más pesados a medida que la demanda avanza.

A la hora de hablar de escalabilidad encontramos dos tipos de soluciones, escalabilidad vertical, que consiste en adquirir un mejor hardware que soporte mi solución o una escalabilidad horizontal, en la cual varias máquinas están corriendo el mismo software y por lo tanto es la solución más utilizada en los últimos tiempos.

La disponibilidad es la capacidad de una aplicación o un servicio de poder estar siempre disponible (24 horas del día), aún cuando suceda un improvisto.

Es mayor la disponibilidad cuando se realiza escalabilidad horizontal

Swarm no ofrece la solución a estos problemas.
Guido Vilariño

# Arquitectura de Docker Swarm

La arquitectura de Swarm tiene un esquema de dos tipos de servidores involucrados: los managers y los workers.

* Los **managers** son encargados de administrar la comunicación entre los contenedores para que sea una unidad homogénea.

* Los **workers** son nodos donde se van a ejecutar contenedores, funciona como un núcleo, los contenedores estarán corriendo en los workers.

**Todos deben tener Docker Daemon (idealmente la misma versión) y deben ser visibles entre sí.**

# Preparando tus aplicaciones para Docker Swarm: los 12 factores

**¿ Está tu aplicación preparada para Docker Swarm ?**
Para saberlo, necesitas comprobarlo con los 12 factores

1. Codebase : el código debe estar en un repositorio
2. Dependencies : deben estar declaradas en un archivo de formato versionable, suele ser un archivo de código
3. Configuration : debe formar parte de la aplicación cuando esté corriendo, puede ser dentro de un archivo
4. Backing services : debe estar conectada a tu aplicación sin que esté dentro, se debe tratar como algo externo
5. Build, release, run : deben estar separadas entre sí.
6. Processes : todos los procesos los puede hacer como una unidad atómica
7. Port binding : tu aplicación debe poder exponerse a sí misma sin necesidad de algo intermediario
8. Concurrency : que pueda correr con múltiples instancias en paralelo
9. Disposabilty : debe estar diseñada para que sea fácilmente destruible
10. Dev/Prod parity : lograr que tu aplicación sea lo más parecido a lo que estará en producción
11. Logs : todos los logs deben tratarse como flujos de bytes
12. Admin processes : la aplicación tiene que poder ser ejecutable como procesos independientes de la aplicación

[Recurso extra](https://12factor.net/)

# Tu primer Docker Swarm

* `docker swarm init`
* `docker node ls`
* `docker node inspect self`
* `docker node inspect --pretty self`
* `docker swarm leave`
* `docker swarm leave --force`

# Fundamentos de Docker Swarm: servicios

No se corren contenedores manualmente, se declaran los servicios que se desean correr

* `docker service create --name pinger alpine ping www.google.com`

# Entendiendo el ciclo de vida de un servicio

Desde el **Cliente** , ‘docker service create’ le envía al **Nodo Manager** el servicio: se crea, se verifican cuántas tareas tendrá, se le otorga una IP virtual y asigna tareas a nodos; esta información es recibida por el **Nodo Worker**, quien prepara la tarea y luego ejecuta los contenedores.

* `docker service ls`
* `docker service rm nombreServicio`

# Un playground de docker swarm gratuito: play-with-docker

¡Play with docker es una herramienta de otro mundo! Te permitirá colaborar entre distintos usuarios en una mismas sesión de docker, y lo mejor, ¡puedes incluir tu propia terminal!

# Administrando servicios en escala

* Cambia el numero de tareas

`docker service scale pinger=5`

* Ver las tareas del servicio

`docker service ps pinger`

* Ver los logs del servicio

`docker service logs -f pinger`

* Ver la configuración del servicio

`docker service inspect pinger`

* actualizar alguna configuración del servicio

`docker service update --args "ping www.amazon.com" pinger`

* realiza rollback o cambia al spec anterior 

`docker service rollback pinger`